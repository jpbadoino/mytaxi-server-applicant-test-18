package com.mytaxi.service.driver;

import java.util.List;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.EntityNotFoundException;

public interface CarService {

	CarDO findCarById(final Long carId) throws EntityNotFoundException;

	CarDO findCarByLicensePlate(final String licensePlate) throws EntityNotFoundException;

	Iterable<CarDO> findAllCars();

	List<CarDO> findByRating(Float rating);

	CarDO createCar(final CarDO car) throws EntityNotFoundException;

	void updateCar(final CarDO car) throws EntityNotFoundException;

	void deleteCar(final Long carId) throws EntityNotFoundException;

}
