package com.mytaxi.datatransferobject;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mytaxi.domainvalue.EngineType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarDTO {
	private Long id;

	private Float rating;
	
	@NotNull(message = "EngineType can not be null!")
	private EngineType engineType;
	
	@NotNull(message = "SeatCount can not be null!")
	private Integer seatCount;

	private Boolean convertible;
	
	@NotNull(message = "LicensePlate can not be null!")
	private String licensePlate;
	
	@NotNull(message = "Manufacturer can not be null!")
	private String manufacturer;

	public CarDTO() {

	}

	public CarDTO(Long id, Float rating, EngineType engineType, Integer seatCount, Boolean convertible,
			String licensePlate, String manufacturer) {
		super();
		this.id = id;
		this.rating = rating;
		this.engineType = engineType;
		this.seatCount = seatCount;
		this.convertible = convertible;
		this.licensePlate = licensePlate;
		this.manufacturer = manufacturer;
	}

	public static CarDTOBuilder newBuilder() {
		return new CarDTOBuilder();
	}

	public CarDTO(CarDTOBuilder CarDTOBuilder) {
		this.id = CarDTOBuilder.id;
		this.rating = CarDTOBuilder.rating;
		this.engineType = CarDTOBuilder.engineType;
		this.seatCount = CarDTOBuilder.seatCount;
		this.convertible = CarDTOBuilder.convertible;
		this.licensePlate = CarDTOBuilder.licensePlate;
		this.manufacturer = CarDTOBuilder.manufacturer;
	}

	public Long getId() {
		return id;
	}

	public Float getRating() {
		return rating;
	}

	public EngineType getEngineType() {
		return engineType;
	}

	public Integer getSeatCount() {
		return seatCount;
	}

	public Boolean getConvertible() {
		return convertible;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public static class CarDTOBuilder {
		private Long id;

		private Float rating;

		private EngineType engineType;

		private Integer seatCount;

		private Boolean convertible;

		private String licensePlate;

		private String manufacturer;

		public CarDTOBuilder id(Long id) {
			this.id = id;
			return this;
		}

		public CarDTOBuilder rating(Float rating) {
			this.rating = rating;
			return this;
		}

		public CarDTOBuilder engineType(EngineType engineType) {
			this.engineType = engineType;
			return this;
		}

		public CarDTOBuilder seatCount(Integer seatCount) {
			this.seatCount = seatCount;
			return this;
		}

		public CarDTOBuilder convertible(Boolean convertible) {
			this.convertible = convertible;
			return this;
		}

		public CarDTOBuilder licensePlate(String licensePlate) {
			this.licensePlate = licensePlate;
			return this;
		}

		public CarDTOBuilder manufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
			return this;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Float getRating() {
			return rating;
		}

		public void setRating(Float rating) {
			this.rating = rating;
		}

		public EngineType getEngineType() {
			return engineType;
		}

		public void setEngineType(EngineType engineType) {
			this.engineType = engineType;
		}

		public Integer getSeatCount() {
			return seatCount;
		}

		public void setSeatCount(Integer seatCount) {
			this.seatCount = seatCount;
		}

		public Boolean getConvertible() {
			return convertible;
		}

		public void setConvertible(Boolean convertible) {
			this.convertible = convertible;
		}

		public String getLicensePlate() {
			return licensePlate;
		}

		public void setLicensePlate(String licensePlate) {
			this.licensePlate = licensePlate;
		}

		public String getManufacturer() {
			return manufacturer;
		}

		public void setManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
		}

		public CarDTO createDCarDTO() {
			return new CarDTO(id, rating, engineType, seatCount, convertible, licensePlate, manufacturer);
		}
	}
}
