package com.mytaxi.test;

import java.time.ZonedDateTime;

import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.CarDTO.CarDTOBuilder;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainobject.ManufacturerDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;

@RunWith(MockitoJUnitRunner.class)
public abstract class TestData {
	public static CarDO getCar() {
		CarDO car = new CarDO();
		car.setId(11L);
		car.setSeatCount(2);
		car.setRating(11.0F);
		car.setDateCreated(ZonedDateTime.now());
		car.setLicensePlate("KL-11 4774");
		car.setEngineType(EngineType.DIESEL.getText());
		car.setConvertible(true);

		ManufacturerDO manufacturer = new ManufacturerDO();
		manufacturer.setName("Audi");
		manufacturer.setId(1L);
		manufacturer.setDateCreated(ZonedDateTime.now());
		car.setManufacturer(manufacturer);
		return car;
	}

	public static ManufacturerDO getManufacturer() {
		ManufacturerDO manufacturer = new ManufacturerDO();
		manufacturer.setDateCreated(ZonedDateTime.now());
		manufacturer.setId(1L);
		manufacturer.setName("Audi");
		return manufacturer;
	}

	public static CarDTO getCarDTO() {
		CarDTOBuilder carDTOBuilder = CarDTO.newBuilder().id(10L).convertible(false).engineType(EngineType.DIESEL)
				.licensePlate("KL-11 4455").manufacturer("Mercedez").rating(4.0f).seatCount(4);
		return carDTOBuilder.createDCarDTO();
	}

	public static DriverDO getDriver() {
		DriverDO driver = new DriverDO("driver-2", "pwd");
		driver.setId(1L);
		driver.setDateCreated(ZonedDateTime.now());
		driver.setDeleted(false);
		driver.setCar(TestData.getCar());
		driver.setOnlineStatus(OnlineStatus.ONLINE);
		GeoCoordinate geoCoordinate = new GeoCoordinate(90, 90);
		driver.setCoordinate(geoCoordinate);
		return driver;
	}

	public static DriverDTO getDriverDTO() {
		GeoCoordinate geoCoordinate = new GeoCoordinate(90, 90);
		return DriverDTO.newBuilder().setId(1L).setPassword("pwd").setUsername("driver-2").setCoordinate(geoCoordinate)
				.setCarLicensePlate("KL-11 4444").createDriverDTO();
	}

}
